## Project setup

```bash
git clone git@bitbucket.org:ograbek/trello_twin.git
yarn
```

Next you need to setup environment variables. To do that, create .env file the root of the project.

```
PORT=5000
DATABASE_URL='postgres://dbuser@localhost:5432/trello_twin_development'
```

## Documentation

### Migrations

To create new migration file run:

```
npm run migrate create name-of-migration-file
```

To run all migrations:

```
npm run migrate up
```

To runs a single down migration:

```
npm run migrate down
```

### Server-side packages

- [node-config](https://github.com/lorenwest/node-config)
- [dotenv](https://github.com/motdotla/dotenv)
- [pg](https://node-postgres.com/)
- [node-pg-migrate](https://github.com/salsita/node-pg-migrate)
