export const SHOW_MODAL = 'SHOW_MODAL';
export const HIDE_MODAL = 'HIDE_MODAL';

export const openModal = (modalType, modalProps = {}) => {
  if (!modalType) throw new Error('Modal type is required!');
  return {
    type: SHOW_MODAL,
    payload: {
      modalType,
      modalProps,
    },
  };
};

export const hideModal = () => {
  return {
    type: HIDE_MODAL,
  };
};
