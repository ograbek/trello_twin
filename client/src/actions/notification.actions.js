export const SHIFT_NOTIFICATION = 'SHIFT_NOTIFICATION';
export const PUSH_NOTIFICATION = 'PUSH_NOTIFICATION';

export function showError(error) {
  return {
    type: PUSH_NOTIFICATION,
    payload: { ...error, notificationType: 'error' },
  };
}

export function showSuccess(message) {
  return {
    type: PUSH_NOTIFICATION,
    payload: { message, notificationType: 'success' },
  };
}

export function shiftNotification() {
  return {
    type: SHIFT_NOTIFICATION,
  };
}
