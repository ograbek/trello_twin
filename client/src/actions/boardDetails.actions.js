import axios from 'axios';

import { showSuccess } from './notification.actions';

export const SET_BOARD = 'SET_BOARD';
export const SET_BOARD_LISTS = 'SET_BOARD_LISTS';
export const ADD_LIST = 'ADD_LIST';
export const SHOW_DELETE_LIST_CONFIRMATION = 'SHOW_DELETE_LIST_CONFIRMATION';
export const DELETE_LIST = 'DELETE_LIST';
export const EDIT_LIST = 'EDIT_LIST';
export const ADD_ITEM = 'ADD_ITEM';
export const SET_ITEMS = 'SET_ITEMS';
export const REORDER_LISTS = 'REORDER_LISTS';

export const fetchBoard = boardId => {
  return async dispatch => {
    const { data } = await axios.get(`/api/boards/${boardId}`);
    dispatch({ type: SET_BOARD, payload: data });
  };
};

export const fetchLists = boardId => {
  return async dispatch => {
    const { data } = await axios.get(`/api/boards/${boardId}/lists`);
    dispatch({ type: SET_BOARD_LISTS, payload: data });
  };
};

export const createList = (boardId, list) => {
  return async dispatch => {
    const { data } = await axios.post(`/api/boards/${boardId}/lists`, list);
    dispatch({ type: ADD_LIST, payload: data });
    dispatch(showSuccess('List was created'));
  };
};

export const editList = list => {
  return async dispatch => {
    const { data } = await axios.put(`/api/lists/${list.id}`, list);
    dispatch({ type: EDIT_LIST, payload: data });
    dispatch(showSuccess('List was updated'));
  };
};

export const deleteList = listId => {
  return async dispatch => {
    await axios.delete(`/api/lists/${listId}`);
    dispatch({ type: DELETE_LIST, payload: listId });
    dispatch(showSuccess('List was deleted'));
  };
};

export const addItem = (listId, item) => {
  return async dispatch => {
    const { data } = await axios.post(`/api/lists/${listId}/items`, item);
    dispatch({ type: ADD_ITEM, payload: { listId, item: data } });
    dispatch(showSuccess('Item was created'));
  };
};

export const fetchItems = listId => {
  return async dispatch => {
    const { data } = await axios.get(`/api/lists/${listId}/items`);
    dispatch({ type: SET_ITEMS, payload: { listId, data } });
  };
};

export const moveItem = (currentListsState, dropResult) => {
  return { type: REORDER_LISTS, payload: dropResult };
};
