import axios from 'axios';
import apiErrorsParser from './../services/apiErrorsParser';
import { showError, showSuccess } from './../actions/notification.actions';

export const FETCHING_BOARD_LIST = 'FETCHING_BOARD_LIST';
export const ADD_BOARD = 'ADD_BOARD';
export const SET_BOARD_LIST = 'SET_BOARD_LIST';
export const EDIT_BOARD = 'EDIT_BOARD';

function dispatchError(dispatch, response) {
  if (!response) return;
  const parsedError = apiErrorsParser(response);
  dispatch(showError(parsedError));
}

export const createBoard = board => {
  return async dispatch => {
    try {
      const { data } = await axios.post('/api/boards', board);
      dispatch({ type: ADD_BOARD, payload: data });
      dispatch(showSuccess('New Board Was Created'));
    } catch ({ response }) {
      dispatchError(dispatch, response);
    }
  };
};

export const fetchBoards = () => async dispatch => {
  dispatch({ type: FETCHING_BOARD_LIST });
  try {
    const { data } = await axios.get('/api/boards');
    dispatch({ type: SET_BOARD_LIST, payload: data });
  } catch ({ response }) {
    dispatchError(dispatch, response);
  }
};

export const editBoard = board => async dispatch => {
  try {
    const { data } = await axios.put(`/api/boards/${board.id}`, board);
    dispatch({ type: EDIT_BOARD, payload: data });
  } catch ({ response }) {
    dispatchError(dispatch, response);
  }
};
