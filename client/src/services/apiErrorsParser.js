function apiErrorsParser({ status, data }) {
  return {
    status,
    message: data.message || 'Something went wrong on the server',
  };
}

export default apiErrorsParser;
