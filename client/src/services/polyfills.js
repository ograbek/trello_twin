if (!Array.prototype.move) {
  Array.prototype.move = function(from, to, count = 1) {
    this.splice(to, 0, ...this.splice(from, count));
  };
}
