import React from 'react';
import PropTypes from 'prop-types';

import './ListItem.css';

const ListItem = props => {
  return (
    <div className="ListItem card z-depth-1" key={props.item.id}>
      {props.item.name}
    </div>
  );
};

ListItem.propTypes = {
  item: PropTypes.object,
};

export default ListItem;
