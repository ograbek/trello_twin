import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { DragDropContext } from 'react-beautiful-dnd';

import ModalRoot from './../ModalRoot/ModalRoot';
import Loader from './../Loader/Loader';
import AddEntity from './../AddEntity/AddEntity';
import ListContainer from './../ListContainer/ListContainer';
import * as boardDetailsActions from './../../actions/boardDetails.actions';
import * as notificationActions from './../../actions/notification.actions';
import * as modalActions from './../../actions/modal.actions';
import { CONFIRMATION_MODAL } from './../ModalRoot/ModalRoot';
import './BoardDetails.css';

class BoardDetails extends Component {
  static propTypes = {
    fetchBoard: PropTypes.func,
    match: PropTypes.object,
    board: PropTypes.object.isRequired,
    showError: PropTypes.func,
    createList: PropTypes.func,
    fetchLists: PropTypes.func,
    lists: PropTypes.array,
    items: PropTypes.object,
    openModal: PropTypes.func,
    hideModal: PropTypes.func,
    deleteList: PropTypes.func,
    editList: PropTypes.func,
    addItem: PropTypes.func,
    fetchItems: PropTypes.func,
    moveItem: PropTypes.func,
  };

  state = { boardId: null };

  componentWillMount() {
    const boardId = this.props.match.params.id;
    this.setState({ boardId });
    this.props.fetchBoard(boardId);
    this.props.fetchLists(boardId);
  }

  onListDelete = ({ id, name }) => {
    this.props.openModal(CONFIRMATION_MODAL, {
      id,
      question: `Are you sure you want to delete "${name}"?`,
      confirm: () => {
        this.props.deleteList(id);
        this.props.hideModal();
      },
    });
  };

  onListSaveError = message => {
    this.props.showError(message);
  };

  onListSave = list => {
    this.props.editList(list);
  };

  onItemSave = (listId, item) => {
    this.props.addItem(listId, item);
  };

  onDragEnd = dropResult => {
    this.props.moveItem(this.props.lists, dropResult);
  };

  render() {
    return (
      <div className="BoardDetails">
        {this.props.board.id && (
          <Fragment>
            <h3>{this.props.board.name}</h3>
            <DragDropContext onDragEnd={this.onDragEnd}>
              <div className="content">
                {this.props.lists.map(list => {
                  return (
                    <div key={list.id}>
                      <ListContainer
                        list={list}
                        onDelete={() => this.onListDelete(list)}
                        onListSaveError={this.onListSaveError}
                        onListSave={this.onListSave}
                        onItemSave={this.onItemSave}
                        fetchItems={this.props.fetchItems}
                        items={this.props.items[list.id] || []}
                      />
                    </div>
                  );
                })}
                <div>
                  <AddEntity
                    onSave={list =>
                      this.props.createList(this.state.boardId, list)
                    }
                    onError={message => this.props.showError({ message })}
                    label="Add New List"
                  />
                </div>
              </div>
            </DragDropContext>
            <ModalRoot />
          </Fragment>
        )}
        {!this.props.board.id && <Loader />}
      </div>
    );
  }
}

function mapStateToProps({ boardDetails }) {
  return { ...boardDetails };
}

export default connect(
  mapStateToProps,
  {
    ...boardDetailsActions,
    ...notificationActions,
    ...modalActions,
  }
)(BoardDetails);
