import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';

import './Notification.css';

class Notification extends Component {
  static propTypes = {
    message: PropTypes.string,
    notificationType: PropTypes.string,
    shiftNotification: PropTypes.func,
  };

  state = { isVisible: true };

  className(isVisible) {
    let modifier = '';
    switch (this.props.notificationType) {
      case 'error':
        modifier = 'red darken-4';
        break;
      case 'success':
        modifier = 'cyan lighten-4';
        break;
      default:
        modifier = '';
        break;
    }
    modifier = isVisible ? modifier : `${modifier} animated slideOutLeft`;
    return `Notification ${modifier}`;
  }

  componentWillMount() {
    this.hideNotificationTimeout = setTimeout(() => {
      this.setState({ isVisible: false });
      this.shiftNotificationTimout = setTimeout(() => {
        this.props.shiftNotification();
      }, 700);
    }, 5000);
  }

  componentWillUnmount() {
    clearTimeout(this.hideNotificationTimeout);
    clearTimeout(this.shiftNotificationTimout);
  }

  render() {
    return (
      <Fragment>
        {this.state.isVisible && (
          <div className={this.className(this.state.isVisible)}>
            {this.props.message}
          </div>
        )}
        {!this.state.isVisible && (
          <div className={this.className(this.state.isVisible)}>
            {this.props.message}
          </div>
        )}
      </Fragment>
    );
  }
}

export default Notification;
