import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';
import { Draggable, Droppable } from 'react-beautiful-dnd';

import './ListContainer.css';
import EntityForm from './../EntityForm/EntityForm';
import ListItem from './../ListItem/ListItem';

class ListContainer extends Component {
  static propTypes = {
    list: PropTypes.object.isRequired,
    onDelete: PropTypes.func.isRequired,
    onListSave: PropTypes.func.isRequired,
    onListSaveError: PropTypes.func.isRequired,
    onItemSave: PropTypes.func.isRequired,
    fetchItems: PropTypes.func.isRequired,
    items: PropTypes.array.isRequired,
  };

  state = {
    editFormVisible: false,
    itemName: '',
  };

  componentDidMount() {
    this.props.fetchItems(this.props.list.id);
  }

  onEditListCancel = () => {
    this.setState({ editFormVisible: false });
  };

  handleItemNameChange = e => {
    this.setState({ itemName: e.target.value });
  };

  handleItemFormSubmit = e => {
    e.preventDefault();
    this.props.onItemSave(this.props.list.id, { name: this.state.itemName });
    this.setState({ itemName: '' });
  };

  renderItem = (item, index) => {
    return (
      <Draggable
        draggableId={`draggable-${item.id}`}
        key={item.id}
        index={index}
      >
        {provided => {
          <ListItem
            item={item}
            ref={provided.innerRef}
            {...provided.draggableProps}
            {...provided.dragHandleProps}
          />;
        }}
      </Draggable>
    );
  };

  getListStyle = () => ({
    overflow: 'auto',
  });

  render() {
    return (
      <article className="ListContainer cyan lighten-5 card">
        {!this.state.editFormVisible && (
          <Fragment>
            <h6 className="title">
              <span className="title-label">{this.props.list.name}</span>
              <div className="list-options">
                <button
                  type="button"
                  className="cyan-text text-darken-4"
                  onClick={() =>
                    this.setState(({ editFormVisible }) => ({
                      editFormVisible: !editFormVisible,
                    }))
                  }
                >
                  <i className="material-icons">edit</i>
                </button>
                <button
                  type="button"
                  className="cyan-text text-darken-4"
                  onClick={() => this.props.onDelete(this.props.list.id)}
                >
                  <i className="material-icons">delete</i>
                </button>
              </div>
            </h6>
            <hr />
          </Fragment>
        )}
        {this.state.editFormVisible && (
          <EntityForm
            onCancel={this.onEditListCancel}
            onSave={this.props.onListSave}
            onError={this.props.onListSaveError}
            entity={this.props.list}
          />
        )}
        <Droppable droppableId={String(this.props.list.id)}>
          {(provided, snapshot) => (
            <div
              ref={provided.innerRef}
              style={this.getListStyle(snapshot.isDraggingOver)}
            >
              {this.props.items.map((item, index) => (
                <Draggable key={item.id} draggableId={item.id} index={index}>
                  {provided => (
                    <div
                      ref={provided.innerRef}
                      {...provided.draggableProps}
                      {...provided.dragHandleProps}
                    >
                      <ListItem item={item} />
                    </div>
                  )}
                </Draggable>
              ))}
              {provided.placeholder}
            </div>
          )}
        </Droppable>
        <form onSubmit={this.handleItemFormSubmit}>
          <input
            type="text"
            placeholder="Add new item..."
            value={this.state.itemName}
            onChange={this.handleItemNameChange}
          />
        </form>
      </article>
    );
  }
}

export default ListContainer;
