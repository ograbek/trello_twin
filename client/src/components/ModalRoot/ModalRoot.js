import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';
import { createPortal } from 'react-dom';
import { connect } from 'react-redux';

import ConfirmatioModal from './../ConfirmationModal/ConfirmationModal';
import './ModalRoot.css';

const modalEl = document.getElementById('modal-root');

export const CONFIRMATION_MODAL = 'confirmation-modal';

class ModalRoot extends Component {
  static propTypes = {
    children: PropTypes.object,
    modalType: PropTypes.string,
    modalProps: PropTypes.object,
  };

  static MODAL_TYPES_MAPPING = {
    [CONFIRMATION_MODAL]: ConfirmatioModal,
  };

  render() {
    if (this.props.modalType) {
      const ModalToShow = ModalRoot.MODAL_TYPES_MAPPING[this.props.modalType];
      return createPortal(
        <Fragment>
          <div className="ModalRoot animated">
            <div className={`modal-container ${this.props.modalType}`}>
              <ModalToShow {...this.props.modalProps}>
                {this.props.children}
              </ModalToShow>
            </div>
          </div>
        </Fragment>,
        modalEl
      );
    }
    return null;
  }
}

function mapStateToProps({ modal }) {
  return { ...modal };
}

export default connect(mapStateToProps)(ModalRoot);
