import React, { Component } from 'react';
import PropTypes from 'prop-types';

import './EntityForm.css';

class EntityForm extends Component {
  static propTypes = {
    onSave: PropTypes.func.isRequired,
    onCancel: PropTypes.func.isRequired,
    onError: PropTypes.func.isRequired,
    entity: PropTypes.object,
  };

  state = { name: '', id: null };

  componentDidMount() {
    if (this.props.entity) {
      const { name, id } = this.props.entity;
      this.setState({ name, id });
    }
  }

  handleInputChange = ({ target: { value, name } }) => {
    this.setState({ [name]: value });
  };

  handleSubmit = e => {
    e.preventDefault();
    if (!this.state.name) {
      this.props.onError('Name can not be blank');
      return;
    }
    this.props.onSave({ name: this.state.name, id: this.state.id });
    this.props.onCancel();
  };

  render() {
    return (
      <form onSubmit={this.handleSubmit} className="EntityForm">
        <div>
          <label htmlFor="name">Name:</label>
          <input
            type="text"
            name="name"
            id="name"
            value={this.state.name}
            onChange={this.handleInputChange}
          />
        </div>
        <a className="cancel" onClick={() => this.props.onCancel()}>
          Cancel
        </a>
        <button
          className="waves-effect waves-light btn-small cyan lighten-4 save"
          type="submit"
        >
          Save
        </button>
      </form>
    );
  }
}

export default EntityForm;
