import React, { Component } from 'react';
import PropTypes from 'prop-types';

import './AddEntity.css';
import EntityForm from './../EntityForm/EntityForm';

class AddEntity extends Component {
  static propTypes = {
    onSave: PropTypes.func.isRequired,
    onError: PropTypes.func.isRequired,
    label: PropTypes.string.isRequired,
  };

  state = { formOpended: false };

  showForm = () => {
    if (!this.state.formOpended) this.toggleForm();
  };

  toggleForm = () => {
    this.setState(currentState => ({ formOpended: !currentState.formOpended }));
  };

  render() {
    return (
      <div className="AddEntity card cyan darken-2" onClick={this.showForm}>
        {!this.state.formOpended && (
          <p className="card-title">{this.props.label}</p>
        )}
        {this.state.formOpended && (
          <EntityForm
            onSave={this.props.onSave}
            onCancel={this.toggleForm}
            onError={this.props.onError}
          />
        )}
      </div>
    );
  }
}

export default AddEntity;
