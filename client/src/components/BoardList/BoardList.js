import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import AddEntity from './../AddEntity/AddEntity';
import * as boardsActions from './../../actions/board.actions';
import * as notificationActions from './../../actions/notification.actions';
import Board from './../Board/Board';
import Loader from './../Loader/Loader';

class BoardList extends Component {
  static propTypes = {
    fetchBoards: PropTypes.func,
    createBoard: PropTypes.func,
    editBoard: PropTypes.func,
    boards: PropTypes.object,
    showError: PropTypes.func,
  };

  componentWillMount() {
    this.props.fetchBoards();
  }

  editBoard = board => {
    this.props.editBoard(board);
  };

  onError = message => {
    this.props.showError({ message });
  };

  render() {
    return (
      <div className="BoardList">
        {!this.props.boards.loadingList && (
          <div className="row">
            {this.props.boards.list.map(board => (
              <article className="col s6 m4 board-item" key={board.id}>
                <Board
                  board={board}
                  onSave={this.editBoard}
                  onError={this.onError}
                />
              </article>
            ))}
            <div className="col s6 m4">
              <AddEntity
                onError={this.onError}
                onSave={this.props.createBoard}
                label="Add New Dashboard"
              />
            </div>
          </div>
        )}

        {this.props.boards.loadingList && (
          <div className="row">
            <Loader />
          </div>
        )}
      </div>
    );
  }
}

function mapStateToProps({ boards }) {
  return { boards };
}

export default connect(mapStateToProps, {
  ...boardsActions,
  ...notificationActions,
})(BoardList);
