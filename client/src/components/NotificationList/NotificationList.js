import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import './NotificationList.css';
import Notification from './../Notification/Notification';
import { shiftNotification } from './../../actions/notification.actions';

class NotificationList extends Component {
  static propTypes = {
    notifications: PropTypes.array,
    shiftNotification: PropTypes.func,
  };

  render() {
    return (
      <div className="NotificationList">
        {this.props.notifications.map((notification, index) => {
          const props = {
            ...notification,
            shiftNotification: this.props.shiftNotification,
          };
          return <Notification {...props} key={index} />;
        })}
      </div>
    );
  }
}

function mapStateToProps({ notifications }) {
  return { notifications };
}

export default connect(mapStateToProps, { shiftNotification })(
  NotificationList
);
