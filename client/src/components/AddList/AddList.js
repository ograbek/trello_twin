import React, { Component } from 'react';

import './AddList.css';

class AddList extends Component {
  render() {
    return (
      <div className="AddList card cyan darken-2">
        <p className="card-title">Add New List</p>
      </div>
    );
  }
}

export default AddList;
