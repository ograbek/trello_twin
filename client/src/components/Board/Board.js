import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

import './Board.css';
import EntityForm from './../EntityForm/EntityForm';

class Board extends Component {
  static propTypes = {
    board: PropTypes.object,
    onSave: PropTypes.func,
    onError: PropTypes.func,
  };

  state = { formOpended: false };

  showForm = () => {
    if (!this.state.formOpended) this.toggleForm();
  };

  toggleForm = () => {
    this.setState(currentState => ({ formOpended: !currentState.formOpended }));
  };

  render() {
    return (
      <div className="Board card cyan lighten-3">
        {!this.state.formOpended && (
          <div>
            <div className="card-content">
              <Link to={`/boards/${this.props.board.id}`}>
                <span className="card-title">{this.props.board.name}</span>
              </Link>
            </div>
            <div className="card-action" onClick={this.showForm}>
              <a className="cyan-text text-darken-4">
                <i className="material-icons">edit</i>
              </a>
            </div>
          </div>
        )}
        {this.state.formOpended && (
          <div className="form-box">
            <EntityForm
              entity={this.props.board}
              onSave={this.props.onSave}
              onCancel={this.toggleForm}
              onError={this.props.onError}
            />
          </div>
        )}
      </div>
    );
  }
}

export default Board;
