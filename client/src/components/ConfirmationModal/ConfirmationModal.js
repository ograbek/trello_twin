import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import * as modalActions from './../../actions/modal.actions';
import './ConfirmationModal.css';

const ConfirmatioModal = props => {
  return (
    <div className="ConfirmatioModal">
      <p className="confirmation">{props.question}</p>
      <div>
        <button
          type="button"
          className="btn-small red darken-4"
          onClick={props.confirm}
        >
          Delete
        </button>
        <button
          type="button"
          className="btn-small grey lighten-1"
          onClick={props.hideModal}
        >
          Cancel
        </button>
      </div>
    </div>
  );
};

ConfirmatioModal.propTypes = {
  question: PropTypes.string.isRequired,
  confirm: PropTypes.func.isRequired,
  hideModal: PropTypes.func,
};

export default connect(null, { ...modalActions })(ConfirmatioModal);
