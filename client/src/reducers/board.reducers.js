import {
  ADD_BOARD,
  SET_BOARD_LIST,
  EDIT_BOARD,
  FETCHING_BOARD_LIST,
} from './../actions';

const defaultState = {
  list: [],
  error: null,
  loadingList: false,
};

export default function boardListReducer(
  store = defaultState,
  { type, payload }
) {
  switch (type) {
    case FETCHING_BOARD_LIST:
      return { ...store, loadingList: true };
    case ADD_BOARD:
      return { ...store, list: [...store.list, payload] };
    case SET_BOARD_LIST:
      return { ...store, list: payload, loadingList: false };
    case EDIT_BOARD: {
      const index = store.list.findIndex(board => board.id === payload.id);
      const list = [
        ...store.list.slice(0, index),
        payload,
        ...store.list.slice(index + 1),
      ];
      return { ...store, list };
    }
    default:
      return store;
  }
}
