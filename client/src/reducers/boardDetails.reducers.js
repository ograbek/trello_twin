import {
  SET_BOARD,
  SET_BOARD_LISTS,
  ADD_LIST,
  DELETE_LIST,
  EDIT_LIST,
  ADD_ITEM,
  SET_ITEMS,
  REORDER_LISTS,
} from './../actions/boardDetails.actions';

const defaultState = {
  board: {},
  lists: [],
  items: {},
};

export default function boardDetailsReducer(
  state = defaultState,
  { type, payload }
) {
  switch (type) {
    case SET_BOARD:
      return { ...state, board: payload };
    case SET_BOARD_LISTS:
      return { ...state, lists: payload };
    case ADD_LIST:
      return { ...state, lists: [...state.lists, payload] };
    case EDIT_LIST: {
      const index = state.lists.findIndex(list => list.id === payload.id);
      const lists = [
        ...state.lists.slice(0, index),
        payload,
        ...state.lists.slice(index + 1),
      ];
      return { ...state, lists };
    }
    case DELETE_LIST:
      return { ...state, lists: state.lists.filter(l => l.id !== payload) };
    case ADD_ITEM: {
      const items = {
        ...state.items,
        [payload.listId]: [...state.items[payload.listId], payload.item],
      };
      return { ...state, items };
    }
    case SET_ITEMS: {
      return {
        ...state,
        items: { ...state.items, [payload.listId]: payload.data },
      };
    }
    case REORDER_LISTS: {
      const { destination, source } = payload;
      if (!destination) return state;
      let listItems = [];
      if (destination.droppableId === source.droppableId) {
        listItems = [...state.items[destination.droppableId]];
        listItems.move(source.index, destination.index);
      }
      return {
        ...state,
        items: { ...state.items, [destination.droppableId]: listItems },
      };
    }
    default:
      return state;
  }
}
