import {
  PUSH_NOTIFICATION,
  SHIFT_NOTIFICATION,
} from './../actions/notification.actions';

const defaultState = [];

export default function notificationReducer(
  state = defaultState,
  { type, payload }
) {
  switch (type) {
    case PUSH_NOTIFICATION:
      return [...state, payload];
    case SHIFT_NOTIFICATION: {
      const newState = state
        .filter(notifiaction => !notifiaction.hidden)
        .map((notifiaction, index) => {
          if (index === 0) notifiaction.hidden = true;
          return notifiaction;
        });
      if (newState.some(notification => !notification.hidden)) {
        return newState;
      } else {
        return [];
      }
    }
    default:
      return state;
  }
}
