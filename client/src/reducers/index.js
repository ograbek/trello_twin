import { combineReducers } from 'redux';
import boardReducer from './board.reducers';
import notificationReducer from './notification.reducers';
import boardDetailsReducer from './boardDetails.reducers';
import modalReducer from './modal.reducers';

export default combineReducers({
  boards: boardReducer,
  notifications: notificationReducer,
  boardDetails: boardDetailsReducer,
  modal: modalReducer,
});
