import { SHOW_MODAL, HIDE_MODAL } from './../actions/modal.actions';

const defaultState = {
  modalType: null,
  modalProps: {},
};

export default function modalReducer(state = defaultState, { type, payload }) {
  switch (type) {
    case SHOW_MODAL:
      return payload;
    case HIDE_MODAL:
      return defaultState;
    default:
      return state;
  }
}
