import React, { Component } from 'react';
import { Link, Route, Switch } from 'react-router-dom';

import './App.css';
import BoardList from './components/BoardList/BoardList';
import BoardDetails from './components/BoardDetails/BoardDetails';
import PageNotFound from './components/PageNotFound/PageNotFound';
import NotificationList from './components/NotificationList/NotificationList';

class App extends Component {
  render() {
    return (
      <div className="App">
        <header className="App-header">
          <Link to="/">
            <i className="large material-icons">home</i>
          </Link>
        </header>
        <div className="container">
          <Switch>
            <Route exact path="/" component={BoardList} />
            <Route path="/boards/:id" component={BoardDetails} />
            <Route component={PageNotFound} />
          </Switch>
        </div>
        <NotificationList />
      </div>
    );
  }
}

export default App;
