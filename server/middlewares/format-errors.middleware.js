function formatErrorsMiddleware() {
  /* eslint-disable-next-line no-unused-vars */
  return function(_err, req, res, next) {
    res
      .status(_err.status)
      .json({ status: _err.status, message: _err.message });
  };
}

module.exports = formatErrorsMiddleware;
