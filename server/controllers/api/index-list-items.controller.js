const db = require('./../../services/db');
const errors = require('./../../services/errors');

async function indexListItemsController(req, res, next) {
  const sql = 'SELECT * FROM items WHERE list_id = $1';
  const values = [req.params.listId];

  try {
    const { rows } = await db.query(sql, values);
    res.json(rows);
  } catch (err) {
    return next(new errors.ServerError());
  }
}

module.exports = indexListItemsController;
