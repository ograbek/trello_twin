const db = require('./../../services/db');
const errors = require('./../../services/errors');

async function createBoardController(req, res, next) {
  if (req.body.name === '') {
    return next(
      new errors.UnprocessableEntityError("The board name can't be blank")
    );
  }
  const sql = `INSERT INTO boards(name) values($1) RETURNING *`;
  try {
    const values = [req.body.name];
    const result = await db.query(sql, values);
    res.json(result.rows[0]);
  } catch (err) {
    return next(new errors.ServerError());
  }
}

module.exports = createBoardController;
