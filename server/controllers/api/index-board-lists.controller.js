const db = require('./../../services/db');
const errors = require('./../../services/errors');

async function indexBoardListsController(req, res, next) {
  const sql = `SELECT * FROM lists WHERE board_id = $1 ORDER BY created_at`;
  try {
    const values = [req.params.boardId];
    const { rows } = await db.query(sql, values);
    res.json(rows);
  } catch (err) {
    return next(new errors.ServerError());
  }
}

module.exports = indexBoardListsController;
