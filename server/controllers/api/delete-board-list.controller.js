const db = require('./../../services/db');
const errors = require('./../../services/errors');

async function deleteBoardListController(req, res, next) {
  const sql = 'DELETE FROM lists WHERE id = $1;';
  const values = [req.params.id];
  try {
    await db.query(sql, values);
  } catch (err) {
    return next(new errors.ServerError());
  }
  res.status(204).send();
}

module.exports = deleteBoardListController;
