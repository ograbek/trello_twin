const db = require('./../../services/db');
const errors = require('./../../services/errors');

async function indexBoardsController(req, res, next) {
  const sql = 'SELECT * FROM boards ORDER BY created_at';
  try {
    const { rows } = await db.query(sql);
    res.json(rows);
  } catch (err) {
    return next(new errors.ServerError());
  }
}

module.exports = indexBoardsController;
