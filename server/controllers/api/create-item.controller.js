const db = require('./../../services/db');
const errors = require('./../../services/errors');

async function createItemController(
  { body: { name }, params: { listId } },
  res,
  next
) {
  if (!name) {
    return next(
      new errors.UnprocessableEntityError('Item name cannot be blank')
    );
  }
  const sql =
    'INSERT INTO items(name, list_id, seq) values($1, $2, $3) RETURNING *';
  let result;

  try {
    const countResult = await db.query(
      'SELECT count(*) from items WHERE list_id = $1',
      [listId]
    );
    result = await db.query(sql, [name, listId, countResult.rows[0].count]);
  } catch (err) {
    return next(new errors.ServerError());
  }
  res.status(200).json(result.rows[0]);
}

module.exports = createItemController;
