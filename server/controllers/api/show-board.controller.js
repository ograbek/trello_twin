const db = require('./../../services/db');
const errors = require('./../../services/errors');

async function showBoardController(req, res, next) {
  const sql = `SELECT * FROM boards WHERE id = $1;`;
  const selectValues = [req.params.id];
  try {
    const { rows } = await db.query(sql, selectValues);
    res.json(rows[0]);
  } catch (err) {
    return next(new errors.ServerError());
  }
}

module.exports = showBoardController;
