const db = require('./../../services/db');
const errors = require('./../../services/errors');

async function editBoardController(req, res, next) {
  if (req.body.name === '') {
    return next(
      new errors.UnprocessableEntityError("The board name can't be blank")
    );
  }

  const selectSql = `SELECT * FROM boards where id = $1;`;
  const selectValues = [req.params.id];
  try {
    var selectResult = await db.query(selectSql, selectValues);
  } catch (err) {
    return next(new errors.ServerError());
  }
  if (selectResult.rows[0]) {
    const updateSql = `UPDATE boards SET name = $1, updated_at = NOW() WHERE id = $2 RETURNING *;`;
    const updateValues = [req.body.name, req.params.id];
    try {
      const updateResult = await db.query(updateSql, updateValues);
      res.json(updateResult.rows[0]);
    } catch (err) {
      return next(new errors.ServerError());
    }
  } else {
    return next(new errors.ResourceNotFoundError());
  }
}

module.exports = editBoardController;
