const db = require('./../../services/db');
const errors = require('./../../services/errors');

async function editListController(req, res, next) {
  if (req.body.name === '') {
    return next("List name can't be blank");
  }

  const selectSql = 'SELECT * FROM lists WHERE id = $1';
  const selectValues = [req.params.id];

  try {
    var result = await db.query(selectSql, selectValues);
  } catch (err) {
    return next(errors.ServerError());
  }

  if (!result.rows[0]) {
    return next(new errors.ResourceNotFoundError());
  }

  const updateSql = `UPDATE lists SET name = $1, updated_at = NOW() WHERE id = $2 RETURNING *;`;
  const updateValues = [req.body.name, req.params.id];

  try {
    const updateResult = await db.query(updateSql, updateValues);
    res.json(updateResult.rows[0]);
  } catch (err) {
    return next(new errors.ServerError());
  }
}

module.exports = editListController;
