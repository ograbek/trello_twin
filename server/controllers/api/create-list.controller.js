const db = require('./../../services/db');
const errors = require('./../../services/errors');

async function createListController(req, res, next) {
  if (req.body.name === '') {
    return next(
      new errors.UnprocessableEntityError("The list name can't be blank")
    );
  }

  const sql = 'INSERT INTO lists(board_id, name) values($1, $2) RETURNING *;';
  try {
    const values = [req.params.boardId, req.body.name];
    const { rows } = await db.query(sql, values);
    res.json(rows[0]);
  } catch (err) {
    return next(new errors.ServerError());
  }
}

module.exports = createListController;
