exports.shorthands = undefined;

exports.up = (pgm) => {
  pgm.createTable('items', {
    id: 'id',
    list_id: {
      type: 'integer',
      notNull: true,
      references: '"lists"',
      onDelete: 'cascade',
    },
    name: { type: 'varchar(255)', notNull: true },
    created_at: {
      type: 'timestamp',
      notNull: true,
      default: pgm.func('current_timestamp'),
    },
    updated_at: {
      type: 'timestamp',
      notNull: true,
      default: pgm.func('current_timestamp'),
    },
  });
  pgm.createIndex('items', 'list_id');
};

exports.down = (pgm) => {
  pgm.dropTable('items');
};
