exports.shorthands = undefined;

exports.up = pgm => {
  pgm.createTable('lists', {
    id: 'id',
    board_id: {
      type: 'integer',
      notNull: true,
      references: '"boards"',
      onDelete: 'cascade',
    },
    name: { type: 'varchar(255)', notNull: true },
    created_at: {
      type: 'timestamp',
      notNull: true,
      default: pgm.func('current_timestamp'),
    },
    updated_at: {
      type: 'timestamp',
      notNull: true,
      default: pgm.func('current_timestamp'),
    },
  });
  pgm.createIndex('lists', 'board_id');
};

exports.down = pgm => {
  pgm.dropTable('lists');
};
