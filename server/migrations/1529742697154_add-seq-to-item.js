exports.shorthands = undefined;

exports.up = pgm => {
  pgm.addColumn('items', {
    seq: {
      type: 'integer',
      notNull: true,
      default: 0,
    },
  });
};

exports.down = pgm => {
  pgm.dropColumns('items', ['seq']);
};
