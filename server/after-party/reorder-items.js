const db = require('./../services/db');

(async () => {
  const listIdSQL = 'SELECT list_id FROM items GROUP BY list_id';
  const { rows } = await db.query(listIdSQL);
  const listIds = rows.map(row => row.list_id);

  const client = await db.connect();
  try {
    await client.query('BEGIN');
    for (let listId of listIds) {
      const { rows } = await db.query(
        'SELECT id, seq from items WHERE list_id = $1 ORDER BY id',
        [listId]
      );
      let index = 0;
      for (let row of rows) {
        await db.query('UPDATE items SET seq = $1 WHERE id = $2', [
          index++,
          row.id,
        ]);
      }
    }
    await client.query('COMMIT');
  } catch (e) {
    await client.query('ROLLBACK');
    throw e;
  } finally {
    client.release();
  }

  process.exit(0);
})().catch(e => {
  /* eslint-disable-next-line no-console */
  console.error(e.stack);
  process.exit(1);
});
