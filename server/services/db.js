const { Pool } = require('pg');
const config = require('./../config');

const pool = new Pool({
  connectionString: config.get('db.url'),
});

pool.on('error', err => {
  /* eslint-disable-next-line no-console */
  console.error(`Unexpected error on idle client ${err}`);
  process.exit(-1);
});

module.exports = pool;
