class BaseError extends Error {
  constructor(message, status) {
    super(message);
    Error.captureStackTrace(this, this.constructor);
    this.name = this.constructor.name;
    this.message = message;
    this.status = status;
  }
}

class ResourceNotFoundError extends BaseError {
  constructor(message) {
    super(message || 'Resource not found', 404);
  }
}

class UnprocessableEntityError extends BaseError {
  constructor(message) {
    super(message || 'Unprocessable Entity', 422);
  }
}

class ServerError extends BaseError {
  constructor(message) {
    super(message || 'Something went wrong', 500);
  }
}

module.exports = {
  BaseError,
  ResourceNotFoundError,
  ServerError,
  UnprocessableEntityError,
};
