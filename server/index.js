const config = require('./config');
const express = require('express');
const bodyParser = require('body-parser');
const router = require('./router');
const formatErrorsMiddleware = require('./middlewares/format-errors.middleware');

const app = express();

app.use(bodyParser.json());
app.use(router);
app.use(formatErrorsMiddleware());

app.listen(config.get('port'), () => {
  /* eslint-disable-next-line no-console */
  console.log(`Listen on port ${config.get('port')}`);
});
