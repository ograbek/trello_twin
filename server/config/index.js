const dotenv = require('dotenv');

if (process.env.NODE_ENV == 'development') {
  dotenv.config();
}

process.env.NODE_CONFIG_DIR = `${__dirname}`;
const config = require('config');

module.exports = config;
