const router = require('express').Router();
const createBoardController = require('./controllers/api/create-board.controller');
const indexBoardsController = require('./controllers/api/index-boards.controller');
const editBoardController = require('./controllers/api/edit-board.controller');
const showBoardController = require('./controllers/api/show-board.controller');
const createListControler = require('./controllers/api/create-list.controller');
const indexBoardListsController = require('./controllers/api/index-board-lists.controller');
const deleteBoardListController = require('./controllers/api/delete-board-list.controller');
const editListController = require('./controllers/api/edit-list.controller');
const createItemController = require('./controllers/api/create-item.controller');
const indexListItemsController = require('./controllers/api/index-list-items.controller');

router.get('/api/boards', indexBoardsController);
router.get('/api/boards/:id', showBoardController);
router.get('/api/boards/:boardId/lists', indexBoardListsController);
router.get('/api/lists/:listId/items', indexListItemsController);

router.post('/api/boards', createBoardController);
router.post('/api/boards/:boardId/lists', createListControler);
router.post('/api/lists/:listId/items', createItemController);

router.put('/api/boards/:id', editBoardController);
router.put('/api/lists/:id', editListController);

router.delete('/api/lists/:id', deleteBoardListController);

module.exports = router;
