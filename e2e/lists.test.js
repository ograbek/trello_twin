const puppeteer = require('puppeteer');
const faker = require('faker');

let page;
let browser;

beforeEach(async () => {
  browser = await puppeteer.launch({
    headless: false,
  });

  page = await browser.newPage();
  page.goto('localhost:3000');
});

afterEach(async () => {
  await browser.close();
});

describe('Adding a new list to the dashboard', () => {
  const newListButtonSelector = '.AddEntity';
  const newListFormSelector = `${newListButtonSelector} .EntityForm`;
  let newListButton;

  beforeEach(async () => {
    await page.waitFor('.board-item:first-of-type');
    await page.click('.board-item:first-of-type .card-content');
    await page.waitFor(newListButtonSelector);
    newListButton = await page.$(newListButtonSelector);
    await page.evaluate(() => window.scrollBy(document.body.scrollWidth, 0));
  });
  
  test('user can see "Add new list" button', async () => {
    expect(newListButton).not.toBeNull();
    const buttonLabel = await page.$eval(`${newListButtonSelector} .card-title`, el => el.textContent);
    expect(buttonLabel).toBe('Add New List')
  });

  describe('when user clicks the button', () => {
    test('he sees the form', async () => {
      await page.click(newListButtonSelector);
      const form = await page.$(newListFormSelector);
      const buttonLabel = await page.$(`${newListButtonSelector} .card-title`);

      expect(buttonLabel).toBeNull();
      expect(form).not.toBeNull();
    });
  });

  describe('when user clicks cancel', () => {
    test('he sees button label, not the form', async () => {
      const cancelButtonSelector = `${newListButtonSelector} .EntityForm .cancel`;
      await page.click(newListButtonSelector);
      const cancelButton = await page.$(cancelButtonSelector);
      expect(cancelButton).not.toBeNull();
      await page.click(cancelButtonSelector);

      const form = await page.$(newListFormSelector);
      const buttonLabel = await page.$(`${newListButtonSelector} .card-title`);
      expect(form).toBeNull();
      expect(buttonLabel).not.toBeNull();
    });
  });

  describe('when user clicks save button', () => {
    const saveBtnSelector = `${newListFormSelector} .save`;

    describe('when the name field is empty', () => {

      test('user sees error notification', async () => {
        await page.click(newListButtonSelector);
        await page.click(saveBtnSelector);
        const notificationText = await page.$eval(
          '.NotificationList .Notification',
          el => el.textContent
        );
        expect(notificationText).toEqual('Name can not be blank');
      });
    });


    describe('when the name field is filled in', () => {
      
      test('user creates a new list', async () => {
        const listTitle = faker.random.words(2);
        await page.click(newListButtonSelector);
        await page.type(`${newListFormSelector} #name`, listTitle, { delay: 50 });
        await page.click(saveBtnSelector);

        await page.waitFor('.NotificationList .Notification');
          const notificationText = await page.$eval(
            '.NotificationList .Notification',
            el => el.textContent
          );
        expect(notificationText).toEqual('List was created');

        await page.evaluate(() => window.scrollBy(document.body.scrollWidth, 0));
        const form = await page.$(newListFormSelector);
        expect(form).toBeNull();

        const newListTitle = await page.$eval(`.ListContainer:last-of-type .title .title-label`, el => el.textContent);
        expect(newListTitle).toBe(listTitle);
      });
    });
  });
});