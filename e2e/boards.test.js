const puppeteer = require('puppeteer');
const faker = require('faker');

let page;
let browser;

beforeEach(async () => {
  browser = await puppeteer.launch({
    headless: false,
  });

  page = await browser.newPage();
  page.goto('localhost:3000');
});

afterEach(async () => {
  await browser.close();
});

describe('"Add New Dashboard" button', () => {
  const addNewDashboardBtnSelector = '.AddEntity';

  beforeEach(async () => {
    await page.waitFor(addNewDashboardBtnSelector);
  });

  test('user can see it on the home page', async () => {
    const addNewDashboardBtn = await page.$(addNewDashboardBtnSelector);
    expect(addNewDashboardBtn).not.toBeNull();
  });

  describe('when user clicks the button', () => {
    const addNewBoardFormSelector = `${addNewDashboardBtnSelector} .EntityForm`;

    beforeEach(async () => {
      await page.click(addNewDashboardBtnSelector);
    });

    test('he sees the form', async () => {
      const form = await page.$(addNewDashboardBtnSelector);
      expect(form).not.toBeNull();
    });

    test('can cancel and close the form', async () => {
      await page.click(`${addNewBoardFormSelector} .cancel`);
      const btnLabel = await page.$(
        `${addNewDashboardBtnSelector} .card-title`
      );
      const form = await page.$(addNewBoardFormSelector);
      expect(btnLabel).not.toBeNull();
      expect(form).toBeNull();
    });

    describe('when user cancels the form and reopen it', () => {
      test('the form is cleared off', async () => {
        await page.type(
          `${addNewBoardFormSelector} #name`,
          faker.random.word()
        );
        await page.click(`${addNewBoardFormSelector} .cancel`);
        await page.click(addNewDashboardBtnSelector);
        const inputValue = await page.$eval(
          `${addNewBoardFormSelector} #name`,
          el => el.value
        );
        expect(inputValue).toEqual('');
      });
    });

    describe('when user save the form', () => {
      const addNewDashboardSaveBtn = `${addNewBoardFormSelector} .save`;

      describe('when the name field is empty', () => {
        test('user sees the error notice', async () => {
          await page.click(addNewDashboardSaveBtn);
          const notificationText = await page.$eval(
            '.NotificationList .Notification',
            el => el.textContent
          );
          expect(notificationText).toEqual('Name can not be blank');
        });
      });

      describe('when the name is not empty', () => {
        const boardTitle = faker.random.words(2);

        test('user creates a new dashboard', async () => {
          await page.type(`${addNewBoardFormSelector} #name`, boardTitle, {
            delay: 50,
          });
          await page.click(addNewDashboardSaveBtn);

          await page.waitFor('.NotificationList .Notification');
          const notificationText = await page.$eval(
            '.NotificationList .Notification',
            el => el.textContent
          );
          expect(notificationText).toEqual('New Board Was Created');
          const form = await page.$(addNewBoardFormSelector);
          expect(form).toBeNull();
          const newAddedBoardTitle = await page.$eval(
            '.board-item:last-of-type .card-title',
            el => el.textContent
          );
          expect(newAddedBoardTitle).toEqual(boardTitle);
        });
      });
    });
  });
});

describe('edit board', () => {
  const lastBoardTitleInputSelector =
    '.board-item:last-of-type .EntityForm #name';

  beforeEach(async () => {
    await page.waitFor('.board-item:last-of-type');
  });

  describe('when user clicks edit button', () => {
    test('can toggle edit form', async () => {
      const lastBoardTitle = await page.$eval(
        '.board-item:last-of-type .card-title',
        el => el.textContent
      );
      await page.click('.board-item:last-of-type .card-action');
      let form = await page.$('.board-item:last-of-type .EntityForm');
      const formInputValue = await page.$eval(
        lastBoardTitleInputSelector,
        el => el.value
      );
      expect(form).not.toBeNull();
      expect(formInputValue).toBe(lastBoardTitle);

      await page.click('.board-item:last-of-type .EntityForm .cancel');
      form = await page.$('.board-item:last-of-type .EntityForm');
      expect(form).toBeNull();
    });
  });

  describe('when user fills the form with a new title and clicks save', () => {
    test('saves board changes', async () => {
      await page.click('.board-item:last-of-type .card-action');
      const newBoardTitle = faker.random.words(2);
      await page.evaluate(() => {
        document.querySelector(
          '.board-item:last-of-type .EntityForm #name'
        ).value =
          '';
      });
      await page.type(lastBoardTitleInputSelector, newBoardTitle, {
        delay: 50,
      });
      await page.click('.board-item:last-of-type .EntityForm .save');
      await page.waitFor(1000);
      const boardTitle = await page.$eval(
        '.board-item:last-of-type .card-title',
        el => el.textContent
      );
      expect(boardTitle).toBe(newBoardTitle);
    });
  });

  describe('when user clears the form and tries to save changes', () => {
    test('shows the error message', async () => {
      await page.click('.board-item:last-of-type .card-action');
      const input = await page.$(lastBoardTitleInputSelector);
      await input.click();
      await input.focus();
      await input.click({ clickCount: 3 });
      await input.press('Backspace');
      await page.click('.board-item:last-of-type .EntityForm .save');

      const notificationText = await page.$eval(
        '.NotificationList .Notification',
        el => el.textContent
      );
      expect(notificationText).toEqual('Name can not be blank');
    });
  });
});
